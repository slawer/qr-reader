package com.syltech.qrreader;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.basewin.aidl.OnBarcodeCallBack;
import com.basewin.aidl.OnPrinterListener;
import com.basewin.define.FontsType;
import com.basewin.services.PrinterBinder;
import com.basewin.services.ServiceManager;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Home extends AppCompatActivity {

    FloatingActionButton btnShow;
    String result = "";
    JSONObject printJson = new JSONObject();
    //private TimerCountTools timeTools;
    TextView scan_timeout;
    private PrinterListener printer_callback = new PrinterListener();
    ConnectionDetector connectionDetector;
    final String VERIFY_URL = "http://5.153.40.138:7067/update_staff_order_status";
    String staffID = "", eatenStatus = "", otherStatus = "", menuItem = "", orderDate = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        scan_timeout = findViewById(R.id.scan_timeout);
        connectionDetector = new ConnectionDetector(this);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            result = bundle.getString("result");

            try {
                JSONObject resultJSON = new JSONObject(result);
                staffID = resultJSON.getString("staff_id");
                eatenStatus = resultJSON.getString("status1");
                otherStatus = resultJSON.getString("status2");
                menuItem = resultJSON.getString("menu_item");
                orderDate = resultJSON.getString("selected_order_date");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (eatenStatus.equals("true")) {
                eatenStatus = "Eaten";
            } else {
                eatenStatus = "Not Eaten";
            }

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Scan Result");
            builder.setNegativeButton("Print", (dialog, which) -> printReceipt( staffID,  menuItem,  orderDate,  eatenStatus));
            builder.setPositiveButton("OK", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.setMessage("Staff ID: " + staffID + "\n" + "Order: " + menuItem + "\n" + "Status: " + eatenStatus + "\n" + "Date: " + orderDate);
            AlertDialog alert = builder.create();
            alert.show();
        }

        btnShow = findViewById(R.id.btnShow);
        btnShow.setOnClickListener(view -> {
            //startActivity(new Intent(Home.this, MainActivity.class));
            //finish();
            openZbar();
        });

    }


    class PrinterListener implements OnPrinterListener {
        private final String TAG = "Print";

        @Override
        public void onStart() {
            // TODO 打印开始
            // Print start
            Log.d("", "start print");
        }

        @Override
        public void onFinish() {
            // TODO 打印结束
            // End of the print
            Log.d("", "pint success");
            //timeTools.stop();
            // Log.d("", "time cost：" + timeTools.getProcessTime());
        }

        @Override
        public void onError(int errorCode, String detail) {
            // TODO 打印出错
            // print error
            Log.d("", "print error" + " errorcode = " + errorCode + " detail = " + detail);
            if (errorCode == PrinterBinder.PRINTER_ERROR_NO_PAPER) {
                Toast.makeText(Home.this, "Paper is running out", Toast.LENGTH_SHORT).show();
            }
            if (errorCode == PrinterBinder.PRINTER_ERROR_OVER_HEAT) {
                Toast.makeText(Home.this, "Printer is overheated. Please wait a while", Toast.LENGTH_SHORT).show();
            }
            if (errorCode == PrinterBinder.PRINTER_ERROR_OTHER) {
                Toast.makeText(Home.this, "There was a problem printing your receipt", Toast.LENGTH_SHORT).show();
            }


        }
    }


    private JSONObject getPrintObject(String test) {
        JSONObject json = new JSONObject();
        try {
            json.put("content-type", "txt");
            json.put("content", test);
            json.put("size", "2");
            json.put("position", "left");
            json.put("offset", "0");
            json.put("bold", "0");
            json.put("italic", "0");
            json.put("height", "-1");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return json;
    }

    public void printReceipt(String staffID, String menuItem, String orderDate, String eatenStatus) {
        try {
            JSONArray printTest = new JSONArray();
            com.basewin.services.ServiceManager.getInstence().getPrinter().setPrintFont(FontsType.simsun);
            com.basewin.services.ServiceManager.getInstence().getPrinter().setPrintFontByAsserts("songti.ttf");
            printTest.put(getPrintObject("-----Newco Catering Services----"));
            printTest.put(getPrintObject("Staff ID - " + staffID));
            printTest.put(getPrintObject("Order - " + menuItem));
            printTest.put(getPrintObject("Order Date - " + orderDate));
            printTest.put(getPrintObject("Status - " + eatenStatus));
            printTest.put(getPrintObject(""));
            printTest.put(getPrintObject(""));
            printTest.put(getPrintObject(""));
            printTest.put(getPrintObject(""));
            printTest.put(getPrintObject(""));
            printTest.put(getPrintObject("---Powered by appsNmobile Slns Ltd---\n"));
            printJson.put("spos", printTest);
            printJson.put("spos", printTest);
            ServiceManager.getInstence().getPrinter().printBottomFeedLine(3);
            Bitmap qr = BitmapFactory.decodeResource(getResources(), R.drawable.test);
            Bitmap[] bitmaps = new Bitmap[]{qr.createScaledBitmap(qr, 240, 240, true)};
            com.basewin.services.ServiceManager.getInstence().getPrinter().print(printJson.toString(), bitmaps, printer_callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openZbar() {
        try {
            ServiceManager.getInstence().getScan().startScanZbar(getTimeOut(), new OnBarcodeCallBack() {
                @Override
                public void onScanResult(String s) {
                    //setShow("Result" + s);
                    sendStaffOrderId(s);
                }

                @Override
                public void onFinish() {
                    //setShow("Scan code failed");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private int getTimeOut() {
        String time = scan_timeout.getText().toString().trim();
        int timeout = 0;
        if (TextUtils.isEmpty(time)) {
            timeout = 10;
        } else {
            try {
                timeout = Integer.parseInt(time);
            } catch (Exception e) {
                timeout = 10;
                System.out.println(e.getMessage());
            }
        }
        return timeout;
    }




    public void sendStaffOrderId(String staffID) {
        if (connectionDetector.isNetworkAvailable()) {
            Ion.with(this)
                    .load("POST", VERIFY_URL)
                    .setLogging("", Log.DEBUG)
                    .setMultipartParameter("staff_id", staffID)
                    .asJsonObject()
                    .setCallback((e, result) -> {
                        if (result != null) {
                            Log.d("Response", result.toString());
                            try {
                                JSONObject loginResp = new JSONObject(result.toString());
                                String respCode = loginResp.getString("resp_code");
                                String respMessage = loginResp.getString("message");
                                if (respCode.equals("005")) {
                                    //progressDialog.dismiss();
                                    //finishActivity(respMessage);
                                    String data = loginResp.getString("data");
                                    Intent intent = new Intent(Home.this, Home.class);
                                    intent.putExtra("result", data);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(Home.this, Home.class);
                                    intent.putExtra("result", "There was a problem verifying staff order");
                                    startActivity(intent);
                                    finish();
                                    //progressDialog.dismiss();
                                    Toast.makeText(this, respMessage, Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e1) {
                                //progressDialog.dismiss();
                                e1.printStackTrace();
                            }
                        } else {
                            //progressDialog.dismiss();
                            Toast.makeText(this, "There was a problem verifying your account. Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Toast.makeText(this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


//
//    private void setShow(String msg) {
//        tv_scan_result.setText(msg);
//    }
//
//


}
