package com.syltech.qrreader;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.koushikdutta.ion.Ion;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    EditText userName, userPIN;
    Button loginButton;
    ConnectionDetector connectionDetector;
    final String LOGIN_URL = "http://5.153.40.138:7067/login";
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).hide();
        registerComponents();
    }


    public boolean validateFieldBounds() {
        return userName.getText().toString().length() > 0 || userPIN.getText().toString().length() > 0;
    }


    public void registerComponents() {
        userName = findViewById(R.id.username);
        userPIN = findViewById(R.id.pin);
        loginButton = findViewById(R.id.btnLogin);


        connectionDetector = new ConnectionDetector(this);
        progressDialog = new ProgressDialog(this);
        loginButton.setOnClickListener(v -> {
            if (validateFieldBounds()) {
                progressDialog.setMessage("Logging In..");
                progressDialog.setTitle("Caterer Login");
                progressDialog.setCancelable(true);
                progressDialog.setIndeterminate(true);
                progressDialog.show();
                makeLogInRequest();
            } else {
                userName.setError("Provide Username");
                userPIN.setError("Provide Password");
            }
        });

    }


    public void makeLogInRequest() {
        if (connectionDetector.isNetworkAvailable()) {
            Ion.with(this)
                    .load("POST", LOGIN_URL)
                    .setLogging("", Log.DEBUG)
                    .setMultipartParameter("username", userName.getText().toString())
                    .setMultipartParameter("pin", userPIN.getText().toString())
                    .asJsonObject()
                    .setCallback((e, result) -> {
                        if (result != null) {
                            Log.d("Response", result.toString());
                            try {
                                JSONObject loginResp = new JSONObject(result.toString());
                                String respCode = loginResp.getString("resp_code");
                                String respMessage = loginResp.getString("message");
                                if (respCode.equals("000")) {
                                    progressDialog.dismiss();
                                    finishActivity(respMessage);
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(this, respMessage, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                progressDialog.dismiss();
                                e1.printStackTrace();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(this, "There was a problem verifying your account. Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "Please Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void finishActivity(String message) {
        startActivity(new Intent(LoginActivity.this, Home.class));
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }
//
//
//    public void printDemo() {
//        try {
//
//            JSONArray printTest = new JSONArray();
//            com.basewin.services.ServiceManager.getInstence().getPrinter().setPrintFont(FontsType.simsun);
//            com.basewin.services.ServiceManager.getInstence().getPrinter().setPrintFontByAsserts("songti.ttf");
//            printTest.put(getPrintObject("------------------Newco Catering Services-------------"));
//            printTest.put(getPrintObject("商品                    单价      数量      价格"));
//            printTest.put(getPrintObject("-------------------Working------------------\n"));
//
//            printJson.put("spos", printTest);
//            printJson.put("spos", printTest);
//            ServiceManager.getInstence().getPrinter().printBottomFeedLine(3);
//            Bitmap qr = BitmapFactory.decodeResource(getResources(), R.drawable.test);
//            Bitmap[] bitmaps = new Bitmap[]{qr.createScaledBitmap(qr, 240, 240, true)};
//            com.basewin.services.ServiceManager.getInstence().getPrinter().print(printJson.toString(), bitmaps, printer_callback);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }

//
//    private JSONObject getPrintObject(String test) {
//        JSONObject json = new JSONObject();
//        try {
//            json.put("content-type", "txt");
//            json.put("content", test);
//            json.put("size", "2");
//            json.put("position", "left");
//            json.put("offset", "0");
//            json.put("bold", "0");
//            json.put("italic", "0");
//            json.put("height", "-1");
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return json;
//    }


//    class PrinterListener implements OnPrinterListener {
//        private final String TAG = "Print";
//
//        @Override
//        public void onStart() {
//            // TODO 打印开始
//            // Print start
//            Log.d("", "start print");
//        }
//
//        @Override
//        public void onFinish() {
//            // TODO 打印结束
//            // End of the print
//            Log.d("", "pint success");
//            //timeTools.stop();
//           // Log.d("", "time cost：" + timeTools.getProcessTime());
//        }
//
//        @Override
//        public void onError(int errorCode, String detail) {
//            // TODO 打印出错
//            // print error
//            Log.d("", "print error" + " errorcode = " + errorCode + " detail = " + detail);
//            if (errorCode == PrinterBinder.PRINTER_ERROR_NO_PAPER) {
//                Toast.makeText(LoginActivity.this, "paper runs out during printing", Toast.LENGTH_SHORT).show();
//            }
//            if (errorCode == PrinterBinder.PRINTER_ERROR_OVER_HEAT) {
//                Toast.makeText(LoginActivity.this, "over heat during printing", Toast.LENGTH_SHORT).show();
//            }
//            if (errorCode == PrinterBinder.PRINTER_ERROR_OTHER) {
//                Toast.makeText(LoginActivity.this, "other error happen during printing", Toast.LENGTH_SHORT).show();
//            }
//
//
//        }
//    }


    //public void
    ///Scan Procedure

//    public void openZxing() {
//        try {
//            ServiceManager.getInstence().getScan().startScan(getTimeOut(), new OnBarcodeCallBack() {
//                @Override
//                public void onScanResult(String s) {
//                    setShow("Result" + s);
//                }
//
//                @Override
//                public void onFinish() {
//                    setShow("Scan code failed");
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    public void openZbar() {
//        try {
//            ServiceManager.getInstence().getScan().startScanZbar(getTimeOut(), new OnBarcodeCallBack() {
//                @Override
//                public void onScanResult(String s) throws RemoteException {
//                    setShow("Result" + s);
//                }
//
//                @Override
//                public void onFinish() throws RemoteException {
//                    setShow("Scan code failed");
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//

//    private int getTimeOut() {
//        String time = tv_scan_timeout.getText().toString().trim();
//        int timeout = 0;
//        if (TextUtils.isEmpty(time)) {
//            timeout = 10;
//        } else {
//            try {
//                timeout = Integer.parseInt(time);
//            } catch (Exception e) {
//                timeout = 10;
//                System.out.println(e.getMessage());
//            }
//        }
//        return timeout;
//    }
//
//
//
//    private void setShow(String msg) {
//        tv_scan_result.setText(msg);
//    }

}
