package com.syltech.qrreader;

import android.app.Application;

import com.basewin.utils.GlobalTransData;
import com.basewin.services.ServiceManager;

public class NewcoApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        ServiceManager.getInstence().init(getApplicationContext());
        GlobalTransData.getInstance().init(getApplicationContext());
    }
}
