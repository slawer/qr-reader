package com.syltech.qrreader;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;
//import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;
//implements ZXingScannerView.ResultHandler

public class MainActivity extends AppCompatActivity {

//    private static final int REQUEST_CAMERA = 1;
//    private ZXingScannerView scannerView;
//    ConnectionDetector connectionDetector;
//    final String VERIFY_URL = "http://5.153.40.138:7067/update_staff_order_status";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        scannerView = new ZXingScannerView(this);
//        connectionDetector = new ConnectionDetector(this);
//        setContentView(scannerView);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//                Toast.makeText(MainActivity.this, "Permission is granted!", Toast.LENGTH_LONG).show();
//            } else {
//                requestPermission();
//            }
//        }
//    }
//
//    private boolean checkPermission() {
//        return (ContextCompat.checkSelfPermission(MainActivity.this,
//                CAMERA) == PackageManager.PERMISSION_GRANTED);
//    }
//
//    private void requestPermission() {
//        ActivityCompat.requestPermissions(MainActivity.this, new String[]{CAMERA}, REQUEST_CAMERA);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//                if (scannerView == null) {
//                    scannerView = new ZXingScannerView(this);
//                    setContentView(scannerView);
//                }
//                scannerView.setResultHandler(this);
//                scannerView.startCamera();
//            } else {
//                requestPermission();
//            }
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        scannerView.stopCamera();
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case REQUEST_CAMERA:
//                if (grantResults.length > 0) {
//                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    if (cameraAccepted) {
//                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(CAMERA)) {
//                                displayAlertMessage("You need to allow access for both permissions", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
//                                            requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
//                                        }
//                                    }
//                                });
//                                return;
//                            }
//                        }
//                    }
//                }
//                break;
//        }
//    }
//
//    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener) {
//        new AlertDialog.Builder(MainActivity.this)
//                .setMessage(message)
//                .setPositiveButton("OK", listener)
//                .setNegativeButton("Cancel", null)
//                .create()
//                .show();
//    }
//
//    @Override
//    public void handleResult(Result result) {
//        String scanResult = result.getText();
//        if (scanResult != null) {
//            sendStaffOrderId(scanResult);
//            //Toast.makeText(this, "" + scanResult, Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//
//    public void sendStaffOrderId(String staffID) {
//        if (connectionDetector.isNetworkAvailable()) {
//            Ion.with(this)
//                    .load("POST", VERIFY_URL)
//                    .setLogging("", Log.DEBUG)
//                    .setMultipartParameter("staff_id", staffID)
//                    .asJsonObject()
//                    .setCallback((e, result) -> {
//                        if (result != null) {
//                            Log.d("Response", result.toString());
//                            try {
//                                JSONObject loginResp = new JSONObject(result.toString());
//                                String respCode = loginResp.getString("resp_code");
//                                String respMessage = loginResp.getString("message");
//                                if (respCode.equals("005")) {
//                                    //progressDialog.dismiss();
//                                    //finishActivity(respMessage);
//                                    String data = loginResp.getString("data");
//                                    Intent intent = new Intent(MainActivity.this, Home.class);
//                                    intent.putExtra("result", "Successful");
//                                    startActivity(intent);
//                                    finish();
//                                } else {
//                                    Intent intent = new Intent(MainActivity.this, Home.class);
//                                    intent.putExtra("result", "There was a problem verifying staff order");
//                                    startActivity(intent);
//                                    finish();
//                                    //progressDialog.dismiss();
//                                    Toast.makeText(this, respMessage, Toast.LENGTH_SHORT).show();
//                                }
//
//
//                            } catch (JSONException e1) {
//                                //progressDialog.dismiss();
//                                e1.printStackTrace();
//                            }
//                        } else {
//                            //progressDialog.dismiss();
//                            Toast.makeText(this, "There was a problem verifying your account. Please try again later", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        } else {
//            Toast.makeText(this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
//        }
//    }


}
